import numpy
import tifffile

tileshape = (2048, 2048)
grid_width = 8
grid_height = 5
scene_width = grid_width * tileshape[0]
scene_height = grid_height * tileshape[1]
channels = 4

data = numpy.random.randint(0, 1024, (channels, scene_height, scene_width), 'uint16')

subresolutions = 2
pixelsize = 0.29  # micrometer

def tiles(data, tileshape):
    for c in range(data.shape[0]):
        for y in range(0, data.shape[1], tileshape[0]):
            for x in range(0, data.shape[2], tileshape[1]):
                yield data[c, y : y + tileshape[0], x : x + tileshape[1]]


with tifffile.TiffWriter('temp.ome.tif', bigtiff=True) as tif:
    metadata = {
        'axes': 'CYX',
        'SignificantBits': 8,
        'TimeIncrement': 0.1,
        'TimeIncrementUnit': 's',
        'PhysicalSizeX': pixelsize,
        'PhysicalSizeXUnit': 'µm',
        'PhysicalSizeY': pixelsize,
        'PhysicalSizeYUnit': 'µm',
        'Channel': {'Name': ['Channel 1', 'Channel 2', 'Channel 3']},
    }
    options = dict(
        photometric='minisblack',
        tile=tileshape,
        compression='deflate',
        resolutionunit='CENTIMETER',
    )
    tif.write(
        tiles(data, tileshape),
        shape=data.shape,
        dtype=data.dtype,
        subifds=subresolutions,
        resolution=(1e4 / pixelsize, 1e4 / pixelsize),
        metadata=metadata,
        **options,
    )
    # write pyramid levels to the two subifds
    for level in range(subresolutions):
        mag = 2 ** (level + 1)
        data_ = data[..., ::mag, ::mag]  # in production use resampling to generate sub-resolution images
        tif.write(
            tiles(data_, tileshape),
            shape=data_.shape,
            dtype=data_.dtype,
            subfiletype=1,
            resolution=(1e4 / mag / pixelsize, 1e4 / mag / pixelsize),
            **options,
        )