import sys
import os
import numpy
import javabridge
import bioformats
import pyvips

## image orientation
# numpy (height = y, width = x)
# pyvips (width = x, height = y)
# pyvips.Image.new_from_array(numpy) - does automatic rotation


if __name__ == "__main__":

    javabridge.start_vm(class_path=bioformats.JARS)
    
    #queue_file='/gpfs/stem/data/Project_Estrus/ISH/Confocal/20220527_SZM_00603_I/20220527_SZM_00603_1b_I.queue'
    file_name='/gpfs/stem/data/Project_Estrus/ISH/Confocal/20220527_SZM_00603_I/20220527_SZM_00603_1b_I.czi'
    print(file_name)
    
    irobj = bioformats.ImageReader(file_name)
    meta = bioformats.get_omexml_metadata(file_name)
    xml = bioformats.omexml.OMEXML(meta)
    size_x = xml.image().Pixels.SizeX
    size_y = xml.image().Pixels.SizeY
    size_c = xml.image().Pixels.SizeC
    umtopx_x = xml.image().Pixels.PhysicalSizeX
    umtopx_y = xml.image().Pixels.PhysicalSizeY
    pixel_type = numpy.uint16
#    if xml.image().Pixels.Type == "uint16":
#        pixel_type = numpy.uint16
    
#    dims_x = 51199
#    dims_y = 36863
#    dims_c = 4


    # read to numpy array
    step = 8
    scene = numpy.zeros((size_y, size_x, size_c), dtype = pixel_type)
#    index_y = numpy.linspace(0, size_y, step + 1, dtype = int)
#    index_x = numpy.linspace(0, size_x, step + 1, dtype = int)
#    span_y = numpy.diff(index_y)
#    span_x = numpy.diff(index_x)
#    for i in range(step):
#        y = index_y[i]
#        h = span_y[i]
#        for j in range(step):
#            x = index_x[j]
#            w = span_x[j]
#            frame, scale = irobj.read(XYWH = (x, y, w, h), wants_max_intensity = True)
#            scene[y : (y + h), x : (x + w), :] = frame
#            print(y, x, h, w, frame.shape)

    print("writing image ...")
    image = pyvips.Image.new_from_array(scene)
    print(image.width, image.height, image.bands)
    image = pyvips.Image.arrayjoin(image, across = 1)
    image.set_type(pyvips.GValue.gint_type, "page-height", size_y)
    image.set_type(pyvips.GValue.gstr_type, "image-description",
    f"""<?xml version="1.0" encoding="UTF-8"?>
    <OME xmlns="http://www.openmicroscopy.org/Schemas/OME/2016-06"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.openmicroscopy.org/Schemas/OME/2016-06 http://www.openmicroscopy.org/Schemas/OME/2016-06/ome.xsd">
    <Image ID="Image:0">
         <!-- Minimum required fields about image dimensions -->
         <Pixels
                 BigEndian="false"
                 DimensionOrder="XYCZT"
                 ID="Pixels:0"
                 Interleaved="true"
                 PhysicalSizeX="{umtopx_x}"
                 PhysicalSizeXUnit="µm"
                 PhysicalSizeY="{umtopx_y}"
                 PhysicalSizeYUnit="µm"
                 SignificantBits="16"
                 SizeC="{size_c}"
                 SizeT="1"
                 SizeX="{size_x}"
                 SizeY="{size_y}"
                 SizeZ="1"
                 Type="uint16">
         </Pixels>
    </Image>
    </OME>""")

    #image.set_type(pyvips.GValue.gstr_type, "image-description", metadata)
    image.write_to_file("outtest.tif", pyramid=True, subifd=True, tile=True, compression="none")



    javabridge.kill_vm()
