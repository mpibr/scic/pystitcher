#!/bin/bash

#SBATCH --nodes=1
#SBATCH --partition=cpus
#SBATCH --time=100:00:00
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=32
#SBATCH --constraint=10G
#SBATCH --job-name=cziStitch
#SBATCH --error=log_error_%j.txt
#SBATCH --output=log_output_%j.txt

echo $SLURM_SUBMIT_DIR
echo "Running on $(hostname)"

path_micromamba="/gpfs/scic/software/biotools/micromamba/bin"
path_czistitcher="/gpfs/scic/data/projects/InstinctSex/pystitcher"
path_cache="/tmp/czistitcher2-cache"
path_venv="${path_czistitcher}/venv_czistitcher2"
input_czi="$1"

# Check if input file exists
if [ ! -f "$input_czi" ]; then
    echo "Error: input CZI file is not valid."
    exit 1
fi

# Record the start time (tic)
start_time=$(date +%s)
echo "Start time: $(date)"

# Run the stitching script with micromamba environment
"${path_micromamba}/micromamba" run \
    -p "${path_venv}" \
    stdbuf -oL python -u "${path_czistitcher}/czistitcher2.py" \
    --input_czi "$input_czi" \
    --threads 16 \
    --blocks 50 \
    --cache "$path_cache"

# Record the end time (toc)
end_time=$(date +%s)
echo "End time: $(date)"

# Calculate and display elapsed time
elapsed_time=$((end_time - start_time))
echo "Elapsed time: ${elapsed_time} seconds"