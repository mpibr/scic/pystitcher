import os
import sys
import glob
import re
from pylibCZIrw import czi as pyczi
import tifffile
from skimage.transform import pyramid_reduce
import numpy as np
from typing import List, Dict, Any, Tuple, Union, Generator, Optional
from concurrent.futures import ThreadPoolExecutor
import threading
from multiprocessing import Value
import argparse


def get_czi_files(file_czi: str) -> Tuple[str, str, List[str]]:
    """
    Parses primary file in CZI experiment and collects a full queue of CZI scene tiles.

    Parameters:
        file_czi (str): Path to the primary CZI file.

    Returns:
        Tuple[str, str, List[str]]: A tuple containing:
            - path_name: the directory path of the CZI file,
            - file_prefix: the file name prefix (excluding .czi),
            - matching_files: a sorted list of matching CZI files.
    """
    path_name = os.path.dirname(file_czi)
    file_name = os.path.basename(file_czi)
    file_prefix = file_name.replace('.czi', '')
    file_pattern = os.path.join(path_name, file_prefix + '*.czi')
    matching_files = sorted(glob.glob(file_pattern))
    return path_name, file_prefix, matching_files



def get_numpy_dtype(pixel_type: str) -> np.dtype:
    """
    Maps a given pixel type string to a corresponding NumPy data type.

    Parameters:
        pixel_type (str): The pixel type as a string, such as 'Gray8' or 'Gray16'.

    Returns:
        np.dtype: The corresponding NumPy data type for the specified pixel type. Defaults to np.uint16 if the pixel type is unknown.
    """
    pixel_type_map = {
        "Gray8": np.uint8,
        "Gray16": np.uint16,
        "Gray32Float": np.float32,
        "Bgr24": np.uint8,  # 3 channels, 8-bit
        "Bgr48": np.uint16,  # 3 channels, 16-bit
        "Bgra32": np.uint8,  # 4 channels, 8-bit
        "Bgra64": np.uint16  # 4 channels, 16-bit
    }
    return pixel_type_map.get(pixel_type, np.uint16)  # Default to uint16 if unknown



def calculate_pyramid_levels(scene_shape: Tuple[int, int, int], 
                             grid_shape: Tuple[int, int], 
                             tile_shape: Tuple[int, int, int, int], 
                             downscale: int = 2) -> Tuple[List[Tuple[int, int, int]], int]:
    """
    Calculates the dimensions of each pyramid level for a given scene, based on
    the scene, grid, and tile shapes, as well as a downscaling factor.

    Parameters:
        scene_shape (Tuple[int, int, int]): Dimensions of the scene (channels, height, width).
        grid_shape (Tuple[int, int]): Dimensions of the grid (height, width).
        tile_shape (Tuple[int, int, int, int]): Dimensions of the tile (z, channels, height, width).
        downscale (int): Downscaling factor applied at each pyramid level. Defaults to 2.

    Returns:
        Tuple[List[Tuple[int, int, int]], int]: A tuple containing:
            - scene_pyramid_levels: List of (channels, height, width) for each pyramid level.
            - num_levels: The total number of pyramid levels.
    """
    scene_pyramid_levels = []
    _, scene_height, scene_width = scene_shape
    grid_height, grid_width = grid_shape
    _, _, tile_height, tile_width = tile_shape
    pyramid_height, pyramid_width = scene_height, scene_width
    num_levels = 0
    while (pyramid_height >= tile_height) or (pyramid_width >= tile_width):
        level_height = int(grid_height * (tile_height / downscale ** num_levels))
        level_width = int(grid_width * (tile_width / downscale ** num_levels))
        scene_pyramid_levels.append((scene_shape[0], level_height, level_width))
        pyramid_height //= downscale
        pyramid_width //= downscale
        num_levels += 1
    return scene_pyramid_levels, num_levels



def read_single_tile(idx_channel: int, idx_grid: int, grid_layout: Dict[int, Any], 
                     tile_shape: Tuple[int, int, int, int], 
                     pixel_dtype: np.dtype) -> np.ndarray:
    """
    Reads a single tile from a given channel and grid index, performing a maximum
    Z projection across all Z planes.

    Parameters:
        idx_channel (int): Channel index to read.
        idx_grid (int): Grid index for the tile.
        grid_layout (Dict[int, Any]): Mapping from grid index to metadata for file access.
        tile_shape (Tuple[int, int, int, int]): Shape of the tile (z, channels, height, width).
        pixel_dtype (np.dtype): Data type for the pixel values.

    Returns:
        np.ndarray: The maximum Z-projected tile image as a 2D array.
    """
    tile_z, _, tile_y, tile_x = tile_shape
    metadata = grid_layout.get(idx_grid, None)
    tile_maxprj = np.zeros((tile_y, tile_x), dtype=pixel_dtype)
    if metadata:
        file_czi = metadata[0]
        with pyczi.open_czi(file_czi) as czidoc:
            for idx_zplane in range(tile_z):
                tile_zcyx = czidoc.read(plane={"Z": idx_zplane, "C": idx_channel}, zoom=1.0)
                if tile_zcyx.ndim == 3 and tile_zcyx.shape[-1] == 1:
                    tile_zcyx = np.squeeze(tile_zcyx, axis=-1)
                tile_maxprj = np.maximum(tile_maxprj, tile_zcyx)
    return tile_maxprj



def reduce_pyramid_tile(tile: np.ndarray, level: int, 
                        pixel_dtype: np.dtype) -> np.ndarray:
    """
    Reduces the size of a tile by downscaling based on the pyramid level.

    Parameters:
        tile (np.ndarray): The tile to be downsampled.
        level (int): Pyramid level for downscaling.
        pixel_dtype (np.dtype): Data type to cast the downsampled tile.

    Returns:
        np.ndarray: The downsampled tile at the specified pyramid level.
    """
    scale_factor = (2 ** level)
    reduced_tile = pyramid_reduce(tile, downscale=scale_factor, preserve_range=True)
    reduced_tile = reduced_tile.astype(pixel_dtype)
    return reduced_tile



def read_metadata(file_queue: List[str]) -> Tuple[List[int], List[int], List[int], np.dtype, np.ndarray]:
    """
    Reads metadata from a queue of CZI files to determine tile shapes, grid layout,
    and scene dimensions, as well as pixel data type and tile bounding boxes.

    Parameters:
        file_queue (List[str]): List of file paths for CZI files.

    Returns:
        Tuple[List[int], List[int], List[int], np.dtype, np.ndarray]: A tuple containing:
            - tile_shape: Dimensions of a single tile [z, channels, height, width].
            - grid_shape: Grid dimensions [grid_y, grid_x].
            - scene_shape: Overall scene dimensions [channels, scene_y, scene_x].
            - pixel_dtype: Data type of the pixel values.
            - tile_bbox: Array with bounding box coordinates for each tile.
    """
    tile_bbox = np.zeros((len(file_queue), 4), dtype=np.int32)

    for idx, file_czi in enumerate(file_queue):
        with pyczi.open_czi(file_czi) as czi_obj:
            z_range = czi_obj.total_bounding_box['Z']
            c_range = czi_obj.total_bounding_box['C']
            y_range = czi_obj.total_bounding_box['Y']
            x_range = czi_obj.total_bounding_box['X']
            pixel_type = czi_obj.pixel_types[0]
            pixel_dtype = get_numpy_dtype(pixel_type)

            tile_shape = [z_range[1] - z_range[0], c_range[1] - c_range[0],
                          y_range[1] - y_range[0], x_range[1] - x_range[0]]

            tile_bbox[idx, :] = [y_range[0], y_range[1], x_range[0], x_range[1]]

    tile_y = tile_shape[2]
    tile_x = tile_shape[3]

    scene_y = np.max(tile_bbox[:, 1]) - np.min(tile_bbox[:, 0])
    scene_x = np.max(tile_bbox[:, 3]) - np.min(tile_bbox[:, 2])

    grid_y = int(np.ceil(scene_y / tile_y))
    grid_x = int(np.ceil(scene_x / tile_x))
    grid_shape = [grid_y, grid_x]
    scene_shape = [tile_shape[1], grid_y * tile_y, grid_x * tile_x]
    
    return tile_shape, grid_shape, scene_shape, pixel_dtype, tile_bbox



def get_pixel_resolution(metadata: Dict[str, Union[dict, list]]) -> Dict[str, Optional[float]]:
    """
    Extracts pixel resolution values for X, Y, and Z axes from metadata and converts
    them to microns if present.

    Parameters:
        metadata (Dict[str, Union[dict, list]]): Metadata dictionary containing resolution information.

    Returns:
        Dict[str, Optional[float]]: A dictionary with resolution values in microns for the X, Y, and Z axes,
        with keys 'X', 'Y', and 'Z'. Returns None for any missing axis resolution.
    """
    scaling = metadata.get('ImageDocument', {}).get('Metadata', {}).get('Scaling', {}).get('Items', {}).get('Distance', [])
    res_x, res_y, res_z = None, None, None

    for item in scaling:
        if item.get('@Id') == 'X':
            res_x = float(item.get('Value'))
        elif item.get('@Id') == 'Y':
            res_y = float(item.get('Value'))
        elif item.get('@Id') == 'Z':
            res_z = float(item.get('Value'))

    return {
        'X': res_x * 1e6 if res_x else None,
        'Y': res_y * 1e6 if res_y else None,
        'Z': res_z * 1e6 if res_z else None
    }



def get_grid_layout(tile_bbox: np.ndarray, file_queue: List[str]) -> Dict[int, Tuple[str, int, int, int]]:
    """
    Computes a grid layout based on the bounding box coordinates of tiles, associating each
    tile with its grid index and file information.

    Parameters:
        tile_bbox (np.ndarray): An array of bounding box coordinates for each tile,
                                with shape (num_tiles, 4).
        file_queue (List[str]): List of file paths for CZI files.

    Returns:
        Dict[int, Tuple[str, int, int, int]]: A dictionary mapping each grid index to a tuple
        containing:
            - file_czi (str): File path of the tile.
            - file_idx (int): File index derived from the file name.
            - grid_idx (int): Calculated grid index within the scene.
            - idx (int): Index of the tile in `file_queue`.
    """
    scene_x_min = np.min(tile_bbox[:, 2])
    scene_x_max = np.max(tile_bbox[:, 3])
    scene_y_min = np.min(tile_bbox[:, 0])
    scene_y_max = np.max(tile_bbox[:, 1])
    tile_x = tile_bbox[0, 3] - tile_bbox[0, 2]
    tile_y = tile_bbox[0, 1] - tile_bbox[0, 0]
    grid_x = int(np.ceil((scene_x_max - scene_x_min) / tile_x))
    grid_y = int(np.ceil((scene_y_max - scene_y_min) / tile_y))
    # grid_len = grid_x * grid_y
    # scene_x = grid_x * tile_x
    # scene_y = grid_y * tile_y
    pattern = re.compile(r'\((\d+)\)\.czi')
    grid_layout = {}
    
    for idx, file_czi in enumerate(file_queue):
        match = pattern.search(file_czi)
        file_idx = int(match.group(1)) if match else 0
        tile_y_min, tile_y_max, tile_x_min, tile_x_max = tile_bbox[idx, :]
        x_anchor = np.abs(tile_x_min - scene_x_min)
        y_anchor = np.abs(tile_y_min - scene_y_min)
        x_idx = int(np.ceil(x_anchor / tile_x)) 
        y_idx = int(np.ceil(y_anchor / tile_y)) 
        grid_idx = y_idx * grid_x + x_idx
        grid_layout[grid_idx] = (file_czi, file_idx, grid_idx, idx)
    
    return grid_layout



def get_bits_per_sample(metadata: dict) -> Optional[int]:
    """
    Extracts the BitsPerSample from the CZI metadata.

    Parameters:
        metadata (dict): The CZI metadata dictionary.

    Returns:
        Optional[int]: The BitsPerSample value if found, otherwise None.
    """
    bits_per_sample = metadata.get('ImageDocument', {}).get('Metadata', {}).get(
        'Information', {}).get('Image', {}).get('ComponentBitCount')
    return int(bits_per_sample) if bits_per_sample is not None else None
    
    

def yield_mmap_buffer(mmap_array: np.memmap, chunk_width: int = 2048, chunk_height: int = 2048) -> Generator[np.ndarray, None, None]:
    """
    Generator to yield sections of a memory-mapped array buffer as tiles.
    
    Parameters:
        mmap_array (np.memmap): Memory-mapped NumPy array of the scene data.
        chunk_width (int): Width of each chunk to yield. Default is 2048.
        chunk_height (int): Height of each chunk to yield. Default is 2048.
    
    Yields:
        np.ndarray: A chunk of the memory-mapped array with shape (chunk_height, chunk_width).
    """
    channels, height, width = mmap_array.shape
    for idx_channel in range(channels):
        for y in range(0, height, chunk_height):
            for x in range(0, width, chunk_width):
                yield mmap_array[idx_channel, y:y + chunk_height, x:x + chunk_width]


def process_tile(level: int, idx_channel: int, idx_grid: int, grid_shape: Tuple[int, int], 
                 grid_layout: Dict[int, Tuple[str, int, int, int]], tile_shape: Tuple[int, int, int, int], 
                 pixel_dtype: np.dtype, scene_mmap: np.memmap, flush_counter: Any) -> None:
    """
    Processes a single tile by reading, projecting, and inserting it into the memory-mapped array.
    
    Parameters:
        level (int): Pyramid level to process; affects downsampling.
        idx_channel (int): Channel index of the tile.
        idx_grid (int): Grid index for the tile within the scene layout.
        grid_shape (Tuple[int, int]): Shape of the grid (rows, columns).
        grid_layout (Dict[int, Tuple[str, int, int, int]]): Mapping from grid index to metadata for file access.
        tile_shape (Tuple[int, int, int, int]): Shape of each tile (z, channels, height, width).
        pixel_dtype (np.dtype): Data type for the pixel values.
        scene_mmap (np.memmap): Memory-mapped array where the processed tile is written.
        flush_counter (Value): Shared counter for tracking the number of processed tiles before flushing to disk.
    
    Returns:
        None
    """
    tile = read_single_tile(idx_channel, idx_grid, grid_layout, tile_shape, pixel_dtype)
    if level > 0:
        tile = reduce_pyramid_tile(tile, level, pixel_dtype)
    
    _, grid_x = grid_shape
    grid_row = idx_grid // grid_x
    grid_col = idx_grid % grid_x
    y_pos = grid_row * tile.shape[1]
    x_pos = grid_col * tile.shape[0]

    scene_mmap[idx_channel, y_pos:y_pos + tile.shape[0], x_pos:x_pos + tile.shape[1]] = tile

    with flush_counter.get_lock():
        flush_counter.value += 1
        if flush_counter.value >= flush_interval:
            with flush_lock:
                scene_mmap.flush()
                flush_counter.value = 0


def clean_cache(scene_pyramid_levels: List[Tuple[int, int, int]], file_name: str, cache_path: str = "./cache") -> None:
    """
    Deletes memory-mapped files created for each pyramid level.
    
    Parameters:
        scene_pyramid_levels (List[Tuple[int, int, int]]): List of resolutions for each pyramid level.
        file_name (str): Base name of the files.
        cache_path (str): Path to the cache directory.
    """
    for level in range(len(scene_pyramid_levels)):
        file_mmap = os.path.join(cache_path, f"{file_name}_{level}.mmap")
        if os.path.exists(file_mmap):
            os.remove(file_mmap)
            print(f"Deleted cache file: {file_mmap}")


def open_pyramid_mmap(scene_pyramid_levels: List[Tuple[int, int, int]], file_name: str, 
                      path_cache: str = "./cache", open_mode: str = "r") -> Dict[int, np.memmap]:
    """
    Opens memory-mapped files for each pyramid level, in read or write mode.
    
    Parameters:
        scene_pyramid_levels (List[Tuple[int, int, int]]): List of resolutions for each pyramid level.
        file_name (str): Base name of the memory-mapped files.
        path_cache (str): Path to cache the memory-mapped files.
        open_mode (str): File access mode ('r' or 'w+').
    
    Returns:
        Dict[int, np.memmap]: Dictionary of memory-mapped arrays for each level.
    """
    pyramid_mmap = {}
    for level, scene_resolution in enumerate(scene_pyramid_levels):
        file_mmap = os.path.join(path_cache, f"{file_name}_{level}.mmap")
        scene_mmap = np.memmap(file_mmap, dtype=pixel_dtype, mode=open_mode, shape=scene_resolution)
        pyramid_mmap[level] = scene_mmap
    return pyramid_mmap


def close_pyramid_mmap(pyramid_mmap: Dict[int, np.memmap]) -> None:
    """
    Closes all memory-mapped arrays in the dictionary.
    
    Parameters:
        pyramid_mmap (Dict[int, np.memmap]): Dictionary of memory-mapped arrays.
    """
    for scene_mmap in pyramid_mmap.values():
        del scene_mmap


def write_ometiff(file_ometiff: str, pixel_bits: int, pixel_resolution: Dict[str, Optional[float]], 
                  pixel_dtype: np.dtype, scene_shape: Tuple[int, int, int], tile_shape: Tuple[int, int, int, int], 
                  scene_pyramid_levels: List[Tuple[int, int, int]], pyramid_mmap: Dict[int, np.memmap]) -> None:
    """
    Writes an OME-TIFF file with pyramid levels using the memory-mapped scene data.
    
    Parameters:
        file_ometiff (str): Output OME-TIFF file path.
        pixel_bits (int): Bit depth per sample.
        pixel_resolution (Dict[str, Optional[float]]): Physical pixel resolution in microns.
        pixel_dtype (np.dtype): Data type of the pixel values.
        scene_shape (Tuple[int, int, int]): Shape of the full scene (channels, height, width).
        tile_shape (Tuple[int, int, int, int]): Shape of individual tiles (z, channels, height, width).
        scene_pyramid_levels (List[Tuple[int, int, int]]): List of pyramid level resolutions.
        pyramid_mmap (Dict[int, np.memmap]): Dictionary of memory-mapped scenes for each level.
    """
    print(f"Writing stitched scene to {file_ometiff}")
    subresolutions = len(scene_pyramid_levels)
    metadata = {
        'axes': 'CYX',
        'SignificantBits': pixel_bits,
        'PhysicalSizeX': pixel_resolution['X'],
        'PhysicalSizeXUnit': 'µm',
        'PhysicalSizeY': pixel_resolution['Y'],
        'PhysicalSizeYUnit': 'µm',
        'Channel': {'Name': [f'Channel {i+1}' for i in range(scene_shape[0])]},
    }

    with tifffile.TiffWriter(file_ometiff, bigtiff=True) as tifWriter:
        for level, level_shape in enumerate(scene_pyramid_levels):
            print(f"Writing resolution level {level}")
            tile_width = tile_shape[2] // (2 ** level)
            tile_height = tile_shape[3] // (2 ** level)
            tifWriter.write(
                data=yield_mmap_buffer(pyramid_mmap[level], tile_width, tile_height),
                dtype=pixel_dtype,
                shape=level_shape,
                subifds=(subresolutions - 1) if level == 0 else 0,
                subfiletype=0 if level == 0 else 1,
                tile=(tile_width, tile_height),
                compression="adobe_deflate",
                metadata=metadata if level == 0 else None,
            )


def parse_arguments() -> argparse.Namespace:
    """
    Parses command-line arguments, checking and creating cache directory if needed.
    
    Returns:
        argparse.Namespace: Parsed arguments.
    """
    parser = argparse.ArgumentParser(description="Process CZI files into a stitched OME-TIFF with pyramidal levels.")
    parser.add_argument("-i", "--input_czi", type=str, required=True, help="Path to the primary CZI file.")
    parser.add_argument("-t", "--threads", type=int, default=4, help="Number of threads to use.")
    parser.add_argument("-b", "--blocks", type=int, default=50, help="Number of tiles to keep in memory before flushing.")
    parser.add_argument("-c", "--cache", type=str, default="./cache", help="Path to the cache directory.")
    
    args = parser.parse_args()
    
    if not os.path.exists(args.cache):
        os.makedirs(args.cache)
        print(f"Created cache directory at: {args.cache}")
    
    return args


if __name__ == "__main__":
    # Parse arguments
    args = parse_arguments()
    
    # Prepare CZI files and metadata
    path_name, file_name, file_queue = get_czi_files(args.input_czi)
    tile_shape, grid_shape, scene_shape, pixel_dtype, tile_bbox = read_metadata(file_queue)
    grid_layout = get_grid_layout(tile_bbox, file_queue)
    with pyczi.open_czi(file_queue[0]) as czi_obj:
        metadata = czi_obj.metadata
        pixel_resolution = get_pixel_resolution(metadata)
        pixel_bits = get_bits_per_sample(metadata)

    # Calculate pyramid levels
    scene_pyramid_levels, subresolutions = calculate_pyramid_levels(scene_shape, grid_shape, tile_shape)
    
    # Debug output
    print(f"path: {path_name}")
    print(f"name: {file_name}")
    print(f"tiles to stitch: {len(file_queue)}")
    print(f"tile shape: {tile_shape}")
    print(f"grid shape: {grid_shape}")
    print(f"scene shape: {scene_shape}")
    print(f"pixel dtype: {pixel_dtype}")
    print(f"grid layout: {len(grid_layout)} keys == {tile_bbox.shape[0]} tiles")
    print("pixel resolution (in microns):", pixel_resolution)
    print("pixel bits per sample:", pixel_bits)

    # Initialize memory-mapped files
    flush_interval = args.blocks
    pyramid_mmap_write = open_pyramid_mmap(scene_pyramid_levels, file_name, path_cache=args.cache, open_mode='w+')
    
    # Set up threading
    flush_lock = threading.Lock()
    grid_len = grid_shape[0] * grid_shape[1]

    # Process tiles in parallel
    with ThreadPoolExecutor(max_workers=args.threads) as executor:
        for level, scene_resolution in enumerate(scene_pyramid_levels):
            print(f"pyramid level {level}: {scene_resolution}")
            scene_mmap = pyramid_mmap_write[level]
            flush_counter = Value("i", 0)
            for idx_channel in range(scene_shape[0]):
                for idx_grid in range(grid_len):
                    executor.submit(process_tile, level, idx_channel, idx_grid, grid_shape,
                                    grid_layout, tile_shape, pixel_dtype, scene_mmap, flush_counter)
            with flush_lock:
                scene_mmap.flush()
    close_pyramid_mmap(pyramid_mmap_write)
    
    # Write OME-TIFF
    pyramid_mmap_read = open_pyramid_mmap(scene_pyramid_levels, file_name, path_cache=args.cache, open_mode='r')
    # file_ometiff = "output_pyramidal.ome.tif"
    file_ometiff = os.path.join(path_name, f"{file_name}_stitched.ome.tif")
    if os.path.exists(file_ometiff):
        print(f"Warning: {file_ometiff} already exists. Terminating to avoid overwrite.")
    else:
        write_ometiff(file_ometiff, pixel_bits, pixel_resolution, pixel_dtype, scene_shape, tile_shape,
                  scene_pyramid_levels, pyramid_mmap_read)
    close_pyramid_mmap(pyramid_mmap_read)
    
    # Clean up cache
    clean_cache(scene_pyramid_levels, file_name, args.cache)