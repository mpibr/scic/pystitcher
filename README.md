# CziStitcher

Reads a list of CZI files and stitches based on viewer coordinates

## Dependency
- [tifffile](https://pypi.org/project/tifffile/)
- [pylibCZIrw](https://pypi.org/project/pylibCZIrw/)
- [scikit-image](https://scikit-image.org/)

## Prepare environment with micromamba
* [install](https://mamba.readthedocs.io/en/latest/installation/micromamba-installation.html) micromamba
* usage [guide](https://mamba.readthedocs.io/en/latest/user_guide/micromamba.html)
* create environment

```
$ micromamba create -p ./venv_czistitcher2
$ micromamba activate -p ./venv_czistitcher2
(venv_czistitcher2) $ micromamba install -f environment.yml
```

* run `czistitcher2`

```
(venv_czistitcher2) $ micromamba run -p venv_czistitcher2 python czistitcher2.py /-i <path_to_czi_files>/image_name.czi
```

* processing

The tool reconstructs the scene from tiles in CZI files. Each tile undergoes maximum projection across z-sections. Stitching relies on the grid layout and occurs during writing to the OME-TIFF file. First, all channels are saved at full resolution, followed by progressively reduced pyramid resolutions.

* arguments

```
 % python czistitcher2.py --help
usage: czistitcher2.py [-h] -i INPUT_CZI [-t THREADS] [-b BLOCKS] [-c CACHE]

Process CZI files into a stitched OME-TIFF with pyramidal levels.

options:
  -h, --help            show this help message and exit
  -i INPUT_CZI, --input_czi INPUT_CZI
                        Path to the primary CZI file.
  -t THREADS, --threads THREADS
                        Number of threads to use.
  -b BLOCKS, --blocks BLOCKS
                        Number of tiles to keep in memory before flushing.
  -c CACHE, --cache CACHE
                        Path to the cache directory.
```

* slurm support

```
$ ssh <user>@slurm.mpibr.local
$ cd /path/czistitcher2
$ bash slurm_czistithcer2.sh <path_to_czi_files>/image_name.czi
$ squeue # to check current object queue
```