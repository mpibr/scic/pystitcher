#!/usr/bin/python3

import sys
import os
import numpy
import javabridge
import bioformats
import pyvips

javabridge.start_vm(class_path=bioformats.JARS)

# create bfreader
czi_file = sys.argv[1]
bfreader = bioformats.ImageReader(czi_file)
meta = bioformats.get_omexml_metadata(czi_file)
xml = bioformats.omexml.OMEXML(meta)
size_x = xml.image().Pixels.SizeX
size_y = xml.image().Pixels.SizeY
size_c = xml.image().Pixels.SizeC
umtopx_x = xml.image().Pixels.PhysicalSizeX
umtopx_y = xml.image().Pixels.PhysicalSizeY
pixel_type = numpy.uint16


# read grid
grid_step = 8
stack = numpy.zeros((size_y, size_x, size_c), dtype = pixel_type)
index_y = numpy.linspace(0, size_y, grid_step + 1, dtype = int)
index_x = numpy.linspace(0, size_x, grid_step + 1, dtype = int)
#index_y = numpy.arange(0, size_y, 2048)
#numpy.append(index_y, size_y)
#index_x = numpy.arange(0, size_x, 2048)
#numpy.append(index_x, size_x)
span_y = numpy.diff(index_y)
span_x = numpy.diff(index_x)
grid_y = len(index_y) - 1
grid_x = len(index_x) - 1

tile_idx = 0
for i in range(grid_step):
    y = index_y[i]
    h = span_y[i]
    for j in range(grid_step):
        x = index_x[j]
        w = span_x[j]
        frame, scale = bfreader.read(XYWH = (x, y, w, h), 
                                     rescale = False, 
                                     wants_max_intensity = True)
        stack[y:(y+h), x:(x+w), :] = frame
        print("tile ", tile_idx, "(x,y,w,h|value):", x, y, w, h, "|",frame.max(), ", shape:", frame.shape)
        tile_idx = tile_idx + 1
javabridge.kill_vm()


for c in range(size_c):
    print("max per stack:", c, stack[:,:,c].max())

image = pyvips.Image.new_from_array(stack, interpretation='grey16')
print("pyvips (w, h, c):", image.width, image.height, image.bands, ", value:", image.max())
out = pyvips.Image.arrayjoin(image, across = 1).cast(pyvips.enums.BandFormat.USHORT)
print("paper-roll (w, h, c):", out.width, out.height, out.bands, ", value:", out.max())

out.set_type(pyvips.GValue.gstr_type, "image-description", xml.to_xml())

# write out file
out_path = os.path.dirname(czi_file)
out_name = os.path.basename(czi_file)
out_name = os.path.splitext(out_name)[0] + "_stitched.tif"
out_file = os.path.join(out_path, out_name)

out.write_to_file(out_file, page_height=size_y,
                   compression="none", subifd=True,
                   pyramid=True, tile=True)


# out.set_type(pyvips.GValue.gstr_type, "image-description",
# f"""<?xml version="1.0" encoding="UTF-8"?>
# <OME xmlns="http://www.openmicroscopy.org/Schemas/OME/2016-06"
# xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
# xsi:schemaLocation="http://www.openmicroscopy.org/Schemas/OME/2016-06 http://www.openmicroscopy.org/Schemas/OME/2016-06/ome.xsd">
# <Image ID="Image:0">
#          <!-- Minimum required fields about image dimensions -->
#          <Pixels
#                  BigEndian="false"
#                  DimensionOrder="XYCZT"
#                  ID="Pixels:0"
#                  Interleaved="true"
#                  PhysicalSizeX="{umtopx_x}"
#                  PhysicalSizeXUnit="µm"
#                  PhysicalSizeY="{umtopx_y}"
#                  PhysicalSizeYUnit="µm"
#                  SignificantBits="16"
#                  SizeC="{size_c}"
#                  SizeT="1"
#                  SizeX="{size_x}"
#                  SizeY="{size_y}"
#                  SizeZ="1"
#                  Type="uint16">
#          </Pixels>
# </Image>
# </OME>""")
